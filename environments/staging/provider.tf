provider "google" {
  version = "~> 3.57.0"
  project = var.project
  region  = var.region

}

provider "google-beta" {
  version = "~> 3.57.0"
  project = var.project
  region  = var.region
}
