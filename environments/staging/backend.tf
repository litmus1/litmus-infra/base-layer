terraform {
  backend "gcs" {
    bucket = "tf-state-litmus-staging"
    prefix = "terraform/state"
  }
}
