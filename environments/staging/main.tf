module staging_environment {
  source                               = "../../modules/environment"
  project                              = var.project
  zone                                 = var.zone
  name                                 = "litmus-staging"
  worker_machine_type                  = "e2-medium"
  preemptible                           = true
  worker_disk_size                     = "20"
  min_node_count                       = 1
  max_node_count                       = 3
}
